       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMI.
       AUTHOR. JARUPHONG.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 WEIGHT     PIC 999V99.
       01 HEIGHT     PIC 999V99.
       01 TOTAL-BMI  PIC 99V99.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter weight (KG) - " WITH NO ADVANCING 
           ACCEPT WEIGHT
           DISPLAY "Enter height (CM) - " WITH NO ADVANCING 
           ACCEPT HEIGHT
           COMPUTE TOTAL-BMI = WEIGHT / (HEIGHT / 100) ** 2
           END-COMPUTE
           DISPLAY "BMI = " TOTAL-BMI
           IF TOTAL-BMI < 18.5 THEN
              DISPLAY "Underweight"
           END-IF
           IF TOTAL-BMI > 18.5 AND < 24.9 THEN
              DISPLAY "Normal"
           END-IF
           IF TOTAL-BMI > 25.0 AND < 29.9 THEN
              DISPLAY "Overweight"
           END-IF
           IF TOTAL-BMI >= 30.0 THEN
              DISPLAY "Obese"
           END-IF
       .